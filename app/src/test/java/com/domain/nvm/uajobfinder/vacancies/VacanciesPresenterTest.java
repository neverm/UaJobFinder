package com.domain.nvm.uajobfinder.vacancies;

import com.domain.nvm.uajobfinder.data.VacanciesRepository;
import com.domain.nvm.uajobfinder.data.Vacancy;
import com.domain.nvm.uajobfinder.data.sync.SyncManager;
import com.domain.nvm.uajobfinder.data.sync.VacanciesSynchronizer;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

import static org.mockito.Mockito.verify;

public class VacanciesPresenterTest {

    @Mock
    private VacanciesContract.View vacanciesView;

    @Mock
    private SyncManager syncManager;

    @Mock
    private VacanciesRepository vacanciesRepository;

    private VacanciesPresenter presenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        presenter = new VacanciesPresenter(vacanciesRepository, syncManager, vacanciesView);
    }

    @Test
    public void createPresenterSetsPresenterToView() {
        verify(vacanciesView).setPresenter(presenter);
    }

    @Test
    public void loadVacanciesCallsShowInView() {
        presenter.loadVacancies();
        verify(vacanciesView).showVacancies(new ArrayList<Vacancy>());
    }
}
