package com.domain.nvm.uajobfinder.vacancies;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.domain.nvm.uajobfinder.Utils;
import com.domain.nvm.uajobfinder.data.VacanciesRepository;
import com.domain.nvm.uajobfinder.data.Vacancy;
import com.domain.nvm.uajobfinder.data.sync.SyncManager;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.schedulers.Schedulers;

public class VacanciesPresenter implements VacanciesContract.Presenter,
        VacanciesRepository.DataObserver {

    private static final String TAG = VacanciesPresenter.class.getSimpleName();

    private VacanciesContract.View vacanciesView;
    private VacanciesRepository repository;
    private SyncManager syncManager;

    @Inject
    public VacanciesPresenter(@NonNull VacanciesRepository vacanciesRepository,
                              @NonNull SyncManager syncManager,
                              @NonNull VacanciesContract.View view) {
        repository = vacanciesRepository;
        repository.subscribe(this);
        this.syncManager = syncManager;
        vacanciesView = view;
        vacanciesView.setPresenter(this);
    }

    @Override
    public void start() {
        syncManager.synchronize(false);
        loadVacancies();
    }

    @Override
    public void detach() {
        repository.unsubscribe(this);
    }

    @Override
    public void loadVacancies() {
        Observable.fromCallable(() -> repository.getVacancies())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onDbLoadSucceeded, this::onDbLoadFailed);
    }

    @Override
    public void openVacancyDetails(@NonNull Vacancy vacancy) {
        vacanciesView.showVacancyDetailsUI(vacancy);
    }

    /**
     * Request synchronization with all remote sources. According to the
     * "Single source of truth" principle this method fetches vacancies and saves them to db
     * and then trigger view update from db.
     * If network connection is not available, show corresponding message in the view
     */
    @Override
    public void requestSync(Context context) {
        if (!Utils.isNetworkAvailable(context)) {
            vacanciesView.showNetworkNotAvailableMessage();
            vacanciesView.hideLoading();
            return;
        }
        vacanciesView.showLoading();
        syncManager.synchronizeAndGetNew(true).observeOn(AndroidSchedulers.mainThread())
                .subscribe(vac -> {}, ex -> {
                    if (ex instanceof CompositeException) {
                        for (Throwable e: ((CompositeException) ex).getExceptions()) {
                            Log.d("SyncManager", e.getMessage());
                        }
                    }
                }, this::loadVacancies);
    }

    @Override
    public void requestClearDb() {
        repository.removeAllVacancies();
    }

    @Override
    public void onDataChanged() {
        loadVacancies();
    }

    private void onDbLoadSucceeded(List<Vacancy> vacancies) {
        if (vacancies.size() > 0) {
            vacanciesView.showVacancies(vacancies);
        }
        else {
            vacanciesView.showNoVacancies();
        }
        vacanciesView.hideLoading();
    }

    private void onDbLoadFailed(Throwable err) {
        Log.e(TAG, "Error loading from db", err);
        vacanciesView.showNoVacancies();
        vacanciesView.hideLoading();
    }
}
