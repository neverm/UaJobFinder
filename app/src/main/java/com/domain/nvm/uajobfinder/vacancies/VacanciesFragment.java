package com.domain.nvm.uajobfinder.vacancies;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.domain.nvm.uajobfinder.R;
import com.domain.nvm.uajobfinder.data.Vacancy;
import com.domain.nvm.uajobfinder.preferences.PreferencesActivity;
import com.domain.nvm.uajobfinder.vacancy_details.VacancyDetailsActivity;

import java.util.List;

public class VacanciesFragment extends Fragment implements VacanciesContract.View {

    private TextView noVacancies;
    private VacanciesContract.Presenter presenter;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;

    public static VacanciesFragment newInstance() {
        return new VacanciesFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_vacancies, container, false);
        noVacancies = (TextView) root.findViewById(R.id.no_vacancies_message);
        recyclerView = (RecyclerView) root.findViewById(R.id.vacancies_list_recycler_view);
        swipeRefreshLayout = (SwipeRefreshLayout) root.findViewById(R.id.vacancies_list_swipe_refresh_layout);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        setHasOptionsMenu(true);
        swipeRefreshLayout.setOnRefreshListener(() -> presenter.requestSync(getActivity()));
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detach();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.vacancy_list, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_vacancy_list_clear_db:
                presenter.requestClearDb();
                return true;
            case R.id.menu_vacancy_list_sync_now:
                presenter.requestSync(getActivity());
                return true;
            case R.id.menu_vacancy_list_settings:
                startActivity(new Intent(getActivity(), PreferencesActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setPresenter(VacanciesContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showVacancies(List<Vacancy> vacancyList) {
        recyclerView.setAdapter(new VacanciesAdapter(vacancyList));
        noVacancies.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showNoVacancies() {
        recyclerView.setAdapter(null);
        recyclerView.setVisibility(View.GONE);
        noVacancies.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoading() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoading() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showVacancyDetailsUI(Vacancy vacancy) {
        Intent i = VacancyDetailsActivity.makeIntent(getActivity(), vacancy);
        startActivity(i);
    }

    @Override
    public void showNetworkNotAvailableMessage() {
        Toast.makeText(getActivity(), R.string.no_connection, Toast.LENGTH_LONG).show();
    }

    private class VacancyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView cityView, titleView, websiteView, isNew;
        private Vacancy vacancy;

        public VacancyHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            cityView = (TextView) itemView.findViewById(R.id.list_item_vacancy_city);
            titleView = (TextView) itemView.findViewById(R.id.list_item_vacancy_title);
            websiteView = (TextView) itemView.findViewById(R.id.list_item_vacancy_website);
            isNew = (TextView) itemView.findViewById(R.id.list_item_vacancy_is_new);
        }

        public void bindVacancy(Vacancy vacancy) {
            cityView.setText(vacancy.getCity());
            titleView.setText(vacancy.getTitle());
            websiteView.setText(vacancy.getWebsite().getName());
            isNew.setText(vacancy.isNew() ? "NEW" : "OLD");
            this.vacancy = vacancy;
        }

        @Override
        public void onClick(View v) {
            presenter.openVacancyDetails(vacancy);
        }
    }

    private class VacanciesAdapter extends RecyclerView.Adapter<VacancyHolder> {

        private List<Vacancy> vacancies;

        public VacanciesAdapter(List<Vacancy> vacancies) {
            this.vacancies = vacancies;
        }

        @Override
        public VacancyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_vacancy, parent, false);
            return new VacancyHolder(view);
        }

        @Override
        public void onBindViewHolder(VacancyHolder holder, int position) {
            holder.bindVacancy(vacancies.get(position));
        }

        @Override
        public int getItemCount() {
            return vacancies.size();
        }
    }
}
