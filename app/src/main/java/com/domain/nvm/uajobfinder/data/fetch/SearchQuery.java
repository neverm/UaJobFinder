package com.domain.nvm.uajobfinder.data.fetch;

public class SearchQuery {

    private String keywords;
    private String city;

    public String getKeywords() {
        return keywords;
    }

    public String getCity() {
        return city;
    }
}
