package com.domain.nvm.uajobfinder.di;

import android.app.Application;
import android.content.Context;

import com.domain.nvm.uajobfinder.data.VacanciesRepository;
import com.domain.nvm.uajobfinder.data.sync.SyncManager;
import com.domain.nvm.uajobfinder.data.sync.SyncService;
import com.domain.nvm.uajobfinder.data.sync.VacanciesSynchronizer;
import com.domain.nvm.uajobfinder.preferences.Preferences;
import com.domain.nvm.uajobfinder.preferences.PreferencesActivity;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules=AppModule.class)
@Singleton
public interface AppComponent {

    void inject(PreferencesActivity.SettingsFragment fragment);
    void inject(SyncService service);

    Application getApp();
    Context getContext();
    Preferences getPreferences();
    VacanciesRepository getRepository();
    SyncManager getSynchronizer();
}
