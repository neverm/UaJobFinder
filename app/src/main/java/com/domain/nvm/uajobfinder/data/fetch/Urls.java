package com.domain.nvm.uajobfinder.data.fetch;

import android.net.Uri;

import com.domain.nvm.uajobfinder.data.Vacancy;
import com.domain.nvm.uajobfinder.data.Website;

public class Urls {
    // todo: put all other url related code and constants here, it's all over the project now
    private static final String DOU_JOB_PAGE_BASE_URL = "https://jobs.dou.ua";
    private static final String RABOTA_JOB_PAGE_BASE_URL = "https://rabota.ua";

    public static Uri buildVacancyUrl(Vacancy vacancy) {
        String relativeUrl = vacancy.getUrl();
        if (!relativeUrl.startsWith("/")) {
            relativeUrl = "/"+relativeUrl;
        }
        String url;
        switch (vacancy.getWebsite()) {
            case RABOTA:
                url = RABOTA_JOB_PAGE_BASE_URL + relativeUrl;
                break;
            case DOU:
            default:
                url =  DOU_JOB_PAGE_BASE_URL + relativeUrl;
        }
        return Uri.parse(url);
    }
}
