package com.domain.nvm.uajobfinder.data.sync;

import android.util.Log;

import com.domain.nvm.uajobfinder.data.VacanciesRepository;
import com.domain.nvm.uajobfinder.data.Vacancy;
import com.domain.nvm.uajobfinder.data.fetch.Fetcher;
import com.domain.nvm.uajobfinder.data.Website;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class VacanciesSynchronizer {

    private static final String TAG = VacanciesSynchronizer.class.getSimpleName();

    private VacanciesRepository repository;

    @Inject
    public VacanciesSynchronizer(VacanciesRepository repository) {
        this.repository = repository;
    }


    /**
     * Reload all vacancies from the given website using given search query
     * @param website
     * @param searchQuery
     * @return fetched vacancies
     */
    // todo: do this in sync manager when search setting for a particular website change
    public Observable<Vacancy> reloadWebsite(Website website, String searchQuery, String city) {
        repository.removeVacanciesFrom(website);
        return syncWebsite(website, searchQuery, city);
    }

    /**
     * Sync with given website using given search query and store results in the local
     * storage
     * @param website
     * @param searchQuery
     * @return new vacancies that haven't been present in local storage before the sync
     */
    public Observable<Vacancy> syncWebsite(Website website, String searchQuery, String city) {
        return updateLocalStorage(fetchVacancies(website, searchQuery, city))
                .filter(Vacancy::isNew);
    }

    /**
     * Fetching result of searching given search query on the given website
     * New vacancies that are not present in the db will be set to new
     * @param website
     * @param searchQuery
     * @return vacancies from the search page of the website
     */
    private Observable<Vacancy> fetchVacancies(Website website, String searchQuery, String city) {

        final Fetcher fetcher = new Fetcher(website);

        return Observable.fromCallable(() -> fetcher.fetchVacancies(searchQuery, city))
                .subscribeOn(Schedulers.io())
                .map(vacs -> markAbsentAsNew(fetcher.getWebsite(), vacs))
                .flatMap(Observable::fromIterable)
                .groupBy(Vacancy::isNew)
                .flatMap(group -> {
                    if (group.getKey()) {
                        return group.flatMap(v -> Observable.just(v)
                                .subscribeOn(Schedulers.io())
                                .map(fetcher::fetchVacancyDetails));
                    } else {
                        return group;
                    }
                });
    }

    /**
     * Update local storage with the given vacancies: add every new vacancy and update every
     * vacancy that is already in the storage
     * @param vacancies
     * @return vacancies that have been given to the method
     */
    private Observable<Vacancy> updateLocalStorage(Observable<Vacancy> vacancies) {
        vacancies.groupBy(Vacancy::isNew)
                .subscribe(group -> {
                    if (!group.getKey()) {
                        group.cache().toList()
                                .subscribe(repository::updateVacancies,
                                err -> logError(err, "failed to update old"));
                    } else {
                        group.cache().toList()
                                .subscribe(repository::saveVacancies,
                                err -> logError(err, "failed to save new"));
                    }
                });
        return vacancies;
    }

    /**
     * Check which vacancies in given list are absent in local database and return a list of
     * vacancies with isNew field set to true for every vacancy absent
     * @param website from which given vacancies were fetched
     * @param vacancies
     * @return updated vacancies with respect to presence in local db
     */
    private List<Vacancy> markAbsentAsNew(Website website, List<Vacancy> vacancies) {
        List<Vacancy> result = new ArrayList<>();
        Set<Vacancy> presentInDb = repository.getPresent(website, vacancies);
        for (Vacancy v: vacancies) {
            Vacancy updatedVacancy = v.setIsNew(!presentInDb.contains(v));
            result.add(updatedVacancy);
        }
        return result;
    }

    private static void logError(Throwable ex, String message) {
        Log.d(TAG, message + " Error message: " + ex.getMessage(), ex);
    }
}
