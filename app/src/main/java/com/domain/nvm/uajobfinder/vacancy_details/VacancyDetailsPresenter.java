package com.domain.nvm.uajobfinder.vacancy_details;

import android.support.annotation.NonNull;

import com.domain.nvm.uajobfinder.data.VacanciesRepository;
import com.domain.nvm.uajobfinder.data.Vacancy;
import com.domain.nvm.uajobfinder.data.fetch.Urls;

import javax.inject.Inject;

public class VacancyDetailsPresenter implements VacancyDetailsContract.Presenter {

    private VacancyDetailsContract.View detailsView;
    private VacanciesRepository repository;

    @Inject
    public VacancyDetailsPresenter(@NonNull VacancyDetailsContract.View detailsView,
                                   @NonNull VacanciesRepository repository) {
        this.detailsView = detailsView;
        this.repository = repository;
        detailsView.setPresenter(this);
    }

    @Override
    public void start() {

    }

    @Override
    public void detach() {

    }

    @Override
    public void openVacancyUrl(@NonNull Vacancy vacancy) {
        detailsView.goToVacancyUrl(Urls.buildVacancyUrl(vacancy));
    }

    @Override
    public void readVacancy(@NonNull Vacancy vacancy) {
        if (vacancy.isNew()) {
            vacancy = vacancy.setIsNew(false);
            repository.updateVacancy(vacancy);
        }
    }
}
