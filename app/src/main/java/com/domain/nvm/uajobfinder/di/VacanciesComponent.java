package com.domain.nvm.uajobfinder.di;

import com.domain.nvm.uajobfinder.vacancies.VacanciesActivity;

import dagger.Component;

@Component(modules = {VacanciesModule.class}, dependencies = {AppComponent.class})
@ActivityScope
public interface VacanciesComponent {

    void inject(VacanciesActivity activity);
}
