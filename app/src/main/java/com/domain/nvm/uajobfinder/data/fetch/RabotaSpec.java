package com.domain.nvm.uajobfinder.data.fetch;

import java.util.regex.Pattern;

public class RabotaSpec implements ParseSpec {

    private static final String BASE_URL = "https://rabota.ua";

    private static final String BASE_SEARCH_URL =
            BASE_URL + "/jobsearch/vacancy_list?keyWords=%s&regionId=%s";

    // search page
    private static final String SELECTOR_VACANCY = "article.f-vacancylist-vacancyblock";
    private static final String SELECTOR_VACANCY_TITLE = "h3.f-vacancylist-vacancytitle";
    private static final String SELECTOR_VACANCY_LINK = "h3.f-vacancylist-vacancytitle a";
    private static final String SELECTOR_VACANCY_CITY = ".f-vacancylist-characs-block .fd-merchant";
    private static final String SELECTOR_VACANCY_SHORT_DESCR = ".f-vacancylist-shortdescr";
    private static final String SELECTOR_VACANCY_COMPANY = ".f-vacancylist-companyname";
    // details page
    private static final String SELECTOR_VACANCY_DESCRIPTION = ".f-vacancy-description";

    private static final Pattern REGEX_RELATIVE_URL = Pattern.compile(".*(/company\\d+/vacancy\\d+)/?");
    private static final Pattern REGEX_ID_COMPANY = Pattern.compile("/company(\\d+)/vacancy\\d+/?");
    private static final Pattern REGEX_ID_VACANCY = Pattern.compile("/company\\d+/vacancy(\\d+)/?");

    @Override
    public String buildSearchUrl(String searchQuery, String regionId) {
        return String.format(BASE_SEARCH_URL, searchQuery, regionId);
    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }

    @Override
    public String getSelectorVacancy() {
        return SELECTOR_VACANCY;
    }

    @Override
    public String getSelectorVacancyTitle() {
        return SELECTOR_VACANCY_TITLE;
    }

    @Override
    public String getSelectorVacancyCity() {
        return SELECTOR_VACANCY_CITY;
    }

    @Override
    public String getSelectorVacancyCompany() {
        return SELECTOR_VACANCY_COMPANY;
    }

    @Override
    public String getSelectorVacancyShortDescr() {
        return SELECTOR_VACANCY_SHORT_DESCR;
    }

    @Override
    public String getSelectorVacancyLink() {
        return SELECTOR_VACANCY_LINK;
    }

    @Override
    public String getSelectorVacancyDescription() {
        return SELECTOR_VACANCY_DESCRIPTION;
    }

    @Override
    public String getBaseSearchUrl() {
        return BASE_SEARCH_URL;
    }

    @Override
    public Pattern getRegexRelativeUrl() {
        return REGEX_RELATIVE_URL;
    }

    @Override
    public Pattern getRegexIdCompany() {
        return REGEX_ID_COMPANY;
    }

    @Override
    public Pattern getRegexIdVacancy() {
        return REGEX_ID_VACANCY;
    }

}
