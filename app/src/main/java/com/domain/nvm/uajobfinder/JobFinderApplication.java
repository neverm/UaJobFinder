package com.domain.nvm.uajobfinder;

import android.app.Application;

import com.domain.nvm.uajobfinder.di.AppComponent;
import com.domain.nvm.uajobfinder.di.AppModule;
import com.domain.nvm.uajobfinder.di.DaggerAppComponent;

public class JobFinderApplication extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        // app module needs Application, we provide it here and build AppComponent with that module
        appComponent = DaggerAppComponent
                .builder()
                .appModule(new AppModule(this))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
