package com.domain.nvm.uajobfinder.data;

import java.io.Serializable;

public class Vacancy implements Serializable {

    /**
     * Immutable data type that represents a single vacancy
     */

    /**
     * id field here represents id in the database, so theoretically there could be two
     * vacancy objects with different id and they could be equal
     * Vacancies are uniquely identified by combination of externalId, companyId and website
     */
    private int id;
    private String externalId;
    private String companyId;
    private String title;
    private String content;
    private String city;
    private String companyName;
    private String url;
    private Website website;
    private String salary;
    private long lastFetchTime;
    private boolean isNew;

    public Vacancy(int id, String externalId, String companyId, String title, String content,
                   String city, String companyName, String url, Website website, long lastFetchTime,
                   boolean isNew) {
        this.id = id;
        this.externalId = externalId;
        this.companyId = companyId;
        this.title = title;
        this.content = content;
        this.city = city;
        this.companyName = companyName;
        this.url = url;
        this.website = website;
        this.lastFetchTime = lastFetchTime;
        this.isNew = isNew;
    }

    public Vacancy(Vacancy that) {
        this.id = that.id;
        this.externalId = that.externalId;
        this.companyId = that.companyId;
        this.title = that.title;
        this.content = that.content;
        this.city = that.city;
        this.companyName = that.companyName;
        this.url = that.url;
        this.website = that.website;
        this.lastFetchTime = that.lastFetchTime;
        this.isNew = that.isNew;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public Vacancy setContent(String content) {
        Vacancy v = new Vacancy(this);
        v.content = content;
        return v;
    }

    public String getCity() {
        return city;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getUrl() {
        return url;
    }

    public String getExternalId() {
        return externalId;
    }

    public Website getWebsite() {
        return website;
    }

    public String getCompanyId() {
        return companyId;
    }

    public long getLastFetchTime() {
        return lastFetchTime;
    }

    public boolean isNew() {
        return isNew;
    }

    public String getSalary() {
        return salary;
    }

    public Vacancy setSalary(String salary) {
        Vacancy v = new Vacancy(this);
        v.salary = salary;
        return v;
    }

    public Vacancy setLastFetchTime(long time) {
        Vacancy v = new Vacancy(this);
        v.lastFetchTime = time;
        return v;
    }

    public Vacancy setIsNew(boolean isNew) {
        Vacancy v = new Vacancy(this);
        v.isNew = isNew;
        return v;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vacancy vacancy = (Vacancy) o;

        if (!getExternalId().equals(vacancy.getExternalId())) return false;
        if (!getCompanyId().equals(vacancy.getCompanyId())) return false;
        return getWebsite().equals(vacancy.getWebsite());

    }

    @Override
    public int hashCode() {
        int result = getExternalId().hashCode();
        result = 31 * result + getCompanyId().hashCode();
        result = 31 * result + getWebsite().hashCode();
        return result;
    }
}
