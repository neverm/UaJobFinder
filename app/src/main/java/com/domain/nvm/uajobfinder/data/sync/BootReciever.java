package com.domain.nvm.uajobfinder.data.sync;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootReciever extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            Intent syncService = SyncService.makeBootSyncIntent(context);
            context.startService(syncService);
        }

    }
}
