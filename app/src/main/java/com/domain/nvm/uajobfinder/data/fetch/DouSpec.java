package com.domain.nvm.uajobfinder.data.fetch;


import org.jsoup.select.Elements;

import java.util.regex.Pattern;

public class DouSpec implements ParseSpec {

    // selectors used for extracting search page vacancy data
    private static final String SELECTOR_VACANCY = ".vacancy";
    private static final String SELECTOR_VACANCY_TITLE = ".title";
    private static final String SELECTOR_VACANCY_CITY = ".cities";
    private static final String SELECTOR_VACANCY_COMPANY = ".company";
    private static final String SELECTOR_VACANCY_SHORT_DESCR = ".sh-info";
    private static final String SELECTOR_VACANCY_LINK = ".title a";
    private static final String SELECTOR_VACANCY_DESCRIPTION = ".l-vacancy";
    // selectors used for cleaning given elements
    // select city and company in the description tag
    private static final String SELECTOR_VACANCY_TITLE_CITY_COMPANY = "span.cities, strong";

    private static final String BASE_URL = "https://jobs.dou.ua";

    private static final String BASE_SEARCH_URL =
            BASE_URL + "/vacancies/?search=%s+%s";

    // let's just hope they don't use anything besides letters, underscores and dashes
    private static final Pattern REGEX_RELATIVE_URL = Pattern.compile(".*(/companies/[\\w-_]+/vacancies/\\d+/?)");
    private static final Pattern REGEX_ID_COMPANY = Pattern.compile("/companies/([\\w-_]+)/vacancies/\\d+/?");
    private static final Pattern REGEX_ID_VACANCY = Pattern.compile("/companies/[\\w-_]+/vacancies/(\\d+)/?");

    @Override
    public String buildSearchUrl(String searchQuery, String city) {
        return String.format(BASE_SEARCH_URL, searchQuery, city);
    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }

    @Override
    public String getSelectorVacancy() {
        return SELECTOR_VACANCY;
    }

    @Override
    public String getSelectorVacancyTitle() {
        return SELECTOR_VACANCY_TITLE;
    }

    @Override
    public String getSelectorVacancyCity() {
        return SELECTOR_VACANCY_CITY;
    }

    @Override
    public String getSelectorVacancyCompany() {
        return SELECTOR_VACANCY_COMPANY;
    }

    @Override
    public String getSelectorVacancyShortDescr() {
        return SELECTOR_VACANCY_SHORT_DESCR;
    }

    @Override
    public String getSelectorVacancyLink() {
        return SELECTOR_VACANCY_LINK;
    }

    @Override
    public String getSelectorVacancyDescription() {
        return SELECTOR_VACANCY_DESCRIPTION;
    }

    @Override
    public String getBaseSearchUrl() {
        return BASE_SEARCH_URL;
    }

    @Override
    public Pattern getRegexRelativeUrl() {
        return REGEX_RELATIVE_URL;
    }

    @Override
    public Pattern getRegexIdCompany() {
        return REGEX_ID_COMPANY;
    }

    @Override
    public Pattern getRegexIdVacancy() {
        return REGEX_ID_VACANCY;
    }

    /**
     * remove em tags that contain question marks as text
     */
    @Override
    public Elements processCityElement(Elements city) {
        Elements emTags = city.select("em");
        if (!emTags.isEmpty()) {
            emTags.remove();
        }
        return city;
    }

    /**
     * remove company name and city name from the title
     */
    @Override
    public Elements processTitleElement(Elements title) {
        Elements cityTags = title.select(SELECTOR_VACANCY_TITLE_CITY_COMPANY);
        if (!cityTags.isEmpty()) {
            cityTags.remove();
        }
        return title;
    }
}
