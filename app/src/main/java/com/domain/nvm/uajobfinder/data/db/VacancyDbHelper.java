package com.domain.nvm.uajobfinder.data.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.domain.nvm.uajobfinder.data.db.VacancyContract.VacancyEntry;

public class VacancyDbHelper extends SQLiteOpenHelper {

    public static final int DB_VERSION = 1;

    public static final String DB_NAME = "jobfinder.db";

    public VacancyDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        final String SQL_CREATE_VACANCIES_TABLE =
                "CREATE TABLE " + VacancyEntry.TABLE_NAME + " (" +
                VacancyEntry._ID + " INTEGER PRIMARY KEY, " +
                VacancyEntry.COL_EXTERNAL_ID + " TEXT NOT NULL, " +
                VacancyEntry.COL_COMPANY_ID + " TEXT NOT NULL, " +
                VacancyEntry.COL_TITLE + " TEXT NOT NULL, " +
                VacancyEntry.COL_CONTENT + " TEXT NOT NULL, " +
                VacancyEntry.COL_CITY + " TEXT NOT NULL, " +
                VacancyEntry.COL_COMPANY_NAME + " TEXT NOT NULL, " +
                VacancyEntry.COL_URL + " TEXT NOT NULL, " +
                VacancyEntry.COL_WEBSITE + " TEXT NOT NULL, " +
                VacancyEntry.COL_SALARY + " TEXT NOT NULL, " +
                VacancyEntry.COL_IS_NEW + " INTEGER NOT NULL, " +
                VacancyEntry.COL_LAST_FETCH_TIME + " TEXT NOT NULL, " +
                // it should be fair to assume that combination of company and vacancy ids
                // allow uniquely identify the vacancy
                // company id is here in case vacancy id is not given, in which case
                // vacancy title will be used
                "CONSTRAINT vacancy_no_dup UNIQUE (" +
                        VacancyEntry.COL_EXTERNAL_ID + ", " +
                        VacancyEntry.COL_WEBSITE + ", " +
                        VacancyEntry.COL_COMPANY_ID + ") ON CONFLICT IGNORE" +
                ");";
        db.execSQL(SQL_CREATE_VACANCIES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
