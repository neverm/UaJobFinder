package com.domain.nvm.uajobfinder.vacancies;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.domain.nvm.uajobfinder.JobFinderApplication;
import com.domain.nvm.uajobfinder.R;
import com.domain.nvm.uajobfinder.di.AppComponent;
import com.domain.nvm.uajobfinder.di.DaggerVacanciesComponent;
import com.domain.nvm.uajobfinder.di.VacanciesComponent;
import com.domain.nvm.uajobfinder.di.VacanciesModule;

import javax.inject.Inject;

public class VacanciesActivity extends AppCompatActivity {

    @Inject
    VacanciesPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_fragment_activity);

        VacanciesFragment fragment =
                (VacanciesFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_container);

        if (fragment == null) {
            fragment = VacanciesFragment.newInstance();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, fragment)
                    .commit();
        }

        AppComponent appComponent =
                ((JobFinderApplication) getApplication()).getAppComponent();
        VacanciesComponent vacanciesComponent = DaggerVacanciesComponent.builder()
                .appComponent(appComponent)
                .vacanciesModule(new VacanciesModule(fragment))
                .build();
        vacanciesComponent.inject(this);
    }
}
