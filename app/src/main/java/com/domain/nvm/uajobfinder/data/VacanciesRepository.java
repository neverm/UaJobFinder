package com.domain.nvm.uajobfinder.data;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.util.Log;

import com.domain.nvm.uajobfinder.data.db.VacancyContract.VacancyEntry;
import com.domain.nvm.uajobfinder.data.db.VacancyDbHelper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

public class VacanciesRepository {

    public interface DataObserver {
        void onDataChanged();
    }

    public class NoVacanciesException extends Exception {}

    private static final String TAG = VacanciesRepository.class.getSimpleName();

    private static final String ORDER_BY_TIME_FETCHED = VacancyEntry.COL_LAST_FETCH_TIME + " DESC";

    private VacancyDbHelper dbHelper;
    private List<DataObserver> observers;

    @Inject
    public VacanciesRepository(@NonNull Context context) {
        dbHelper = new VacancyDbHelper(context.getApplicationContext());
        observers = new ArrayList<>();
    }

    private void notifyObservers() {
        for (DataObserver observer: observers) {
            observer.onDataChanged();
        }
    }

    public void subscribe(DataObserver observer) {
        observers.add(observer);
    }

    public void unsubscribe(DataObserver observer) {
        observers.remove(observer);
    }

    public List<Vacancy> getVacancies() throws NoVacanciesException {

        List<Vacancy> vacanciesFromDb = new ArrayList<>();
        Cursor c = dbHelper.getReadableDatabase().query(VacancyEntry.TABLE_NAME, null, null,
                null, null, null, ORDER_BY_TIME_FETCHED);
        while (c.moveToNext()) {
            vacanciesFromDb.add(Utils.vacancyFromCursor(c));
        }
        c.close();
        if (vacanciesFromDb.size() > 0) {
            return vacanciesFromDb;
        }
        else {
            throw new NoVacanciesException();
        }
    }

    public void updateVacancies(List<Vacancy> vacancies) {
        Log.d(TAG, "Updating: " + vacancies.toString());
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.beginTransaction();
        for (Vacancy vac: vacancies) {
            if (vac.getId() != -1) {
                db.update(VacancyEntry.TABLE_NAME, Utils.makeContentValues(vac),
                        VacancyEntry._ID + " = ?",
                        new String[] {Integer.toString(vac.getId())});
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        notifyObservers();
    }

    /**
     * Save given list of vacancies into db. Since database has unique constraints on some fields
     * it's not guaranteed that all vacancies will be saved
     * @param vacancies to save
     * @return list of vacancies that have been successfully added to db
     */
    public List<Vacancy> saveVacancies(List<Vacancy> vacancies) {
        Log.d(TAG, "Saving: " + vacancies.toString());
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Set<Long> savedIds = new HashSet<>();
        List<Vacancy> saved = new ArrayList<>();
        db.beginTransaction();
        for (Vacancy vac: vacancies) {
            long id = db.insert(VacancyEntry.TABLE_NAME, null, Utils.makeContentValues(vac));
            savedIds.add(id);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        notifyObservers();
        for (Vacancy v: vacancies) {
            if (savedIds.contains(v.getId())) {
                saved.add(v);
            }
        }
        return saved;
    }

    public void removeVacanciesFrom(Website website) {
        dbHelper.getWritableDatabase().delete(VacancyEntry.TABLE_NAME,
                VacancyEntry.COL_WEBSITE + " = ?",
                new String[]{website.getName()});
        notifyObservers();
    }

    public void removeAllVacancies() {

        dbHelper.getWritableDatabase().delete(VacancyEntry.TABLE_NAME, null, null);
        notifyObservers();
    }

    /**
     * Update the record in database, corresponding to this vacancy with the values it has now
     * @param vacancy
     */
    public void updateVacancy(Vacancy vacancy) {
        if (vacancy.getId() == -1) {
            throw new SQLException("Trying to update row that doesn't exist");
        }
        int res = dbHelper.getWritableDatabase()
                .update(VacancyEntry.TABLE_NAME, Utils.makeContentValues(vacancy),
                        VacancyEntry._ID + " = ?",
                        new String[] {Integer.toString(vacancy.getId())});
        notifyObservers();
    }

    /**
     * Given list of vacancies, find subset of it that is present in db
     * @param website from which the vacancy has come. Assume that every vacancy we want to consider
     *                as present have their field/db column set to this value
     * @param vacancies
     * @return set of vacancies present in given list and in local database
     */
    public Set<Vacancy> getPresent(Website website, List<Vacancy> vacancies) {
        // todo: explain how it works
        Set<Vacancy> present = new HashSet<>();
        if (vacancies.isEmpty()) {
            return present;
        }
        String whereClause = VacancyEntry.COL_WEBSITE + " = '" + website.getName() + "'";
        whereClause += " AND " + VacancyEntry.COL_EXTERNAL_ID + " IN (%s);";
        String ids = "";
        // todo: String.join, anyone?
        for (Vacancy v: vacancies) {
            ids += v.getExternalId() + ",";
        }
        ids = ids.substring(0, ids.length()-1); // cut off the last comma :-)
        whereClause = String.format(whereClause, ids);

        Cursor c = dbHelper.getReadableDatabase().query(VacancyEntry.TABLE_NAME, null, whereClause,
                null, null, null, null);
        while (c.moveToNext()) {
            Vacancy v = Utils.vacancyFromCursor(c);
            present.add(v);
        }
        c.close();
        return present;
    }
}
