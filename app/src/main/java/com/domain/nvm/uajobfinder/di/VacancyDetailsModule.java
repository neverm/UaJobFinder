package com.domain.nvm.uajobfinder.di;

import com.domain.nvm.uajobfinder.vacancy_details.VacancyDetailsContract;

import dagger.Module;
import dagger.Provides;

// todo: move these classes to corresponding feature package,
// or make them generic presenter module/presenter component via type parameters
@Module
public class VacancyDetailsModule {

    private VacancyDetailsContract.View view;

    public VacancyDetailsModule(VacancyDetailsContract.View v) {
        view = v;
    }

    @Provides
    @ActivityScope
    VacancyDetailsContract.View provideView() {
        return view;
    }
}
