package com.domain.nvm.uajobfinder.preferences;

import android.content.Context;
import android.preference.EditTextPreference;
import android.text.TextUtils;
import android.util.AttributeSet;

/**
 * Merci beaucoup to this gentleman for the idea
 * https://stackoverflow.com/questions/6703130/how-to-update-edittextpreference-existing-summary-when-i-click-ok-button
 */

public class FriendlyEditTextPreference extends EditTextPreference {

    public FriendlyEditTextPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public FriendlyEditTextPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FriendlyEditTextPreference(Context context) {
        super(context);
    }

    /**
     * Return the value of the preference if it's set
     * otherwise return hint summary value.
     * @return string that should be used as summary for this preference
     */
    @Override
    public CharSequence getSummary() {
        String text = getText();
        if (!TextUtils.isEmpty(text)) {
            return text;
        }
        CharSequence summary = super.getSummary();
        if (!TextUtils.isEmpty(summary)) {
            return summary;
        }
        else {
            return super.getSummary();
        }
    }
}
