package com.domain.nvm.uajobfinder.data.fetch;

import android.util.Log;

import com.domain.nvm.uajobfinder.data.Vacancy;
import com.domain.nvm.uajobfinder.data.Website;
import com.domain.nvm.uajobfinder.preferences.Preferences;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Fetcher {

    static class FetchErrorException extends Exception {
        public FetchErrorException(String msg) { super(msg);}
    }

    static class ParseErrorException extends Exception { }

    private static final String TAG = Fetcher.class.getSimpleName();

    private Website website;
    private ParseSpec spec;

    public Fetcher(Website website) {
        this.website = website;
        switch (website) {
            case RABOTA:
                this.spec = new RabotaSpec();
                break;
            case DOU:
            default:
                this.spec = new DouSpec();
        }
    }

    /**
     * @return the website this fetcher is working with
     */
    public Website getWebsite() {
        return website;
    }

    /**
     * Get list of vacancies from the search page for a given search query
     * Vacancy details field will be empty
     * Postcondition:
     * For any two vacancies a, b on the search page, if a was fetched before b,
     * a.getLastFetchTime() should be bigger than b.getLastFetchTime()
     * Informally, this means:
     * 1. All vacancies in the resulting list will have a unique lastFetchTimeValue
     * 2. If the resulting list is sorted by lastFetchTime in descending order, it will be
     * the order in which vacancies were originally presented on the search page
     * @param searchQuery
     * @param city
     * @return
     */
    public List<Vacancy> fetchVacancies(String searchQuery, String city) throws FetchErrorException {
        try {
            if (searchQuery.equals(Preferences.EMPTY_QUERY)) {
                throw new FetchErrorException("Empty search query");
            }
            return parseSearchPage(searchQuery, city);
        }
        catch (IOException ex) {
            throw new FetchErrorException("Fetching search page error: " +
                    "io exception " + ex.getMessage());
        }
    }

    /**
     * Fetch details for the given vacancy and return updated vacancy
     * @param vacancy
     */
    public Vacancy fetchVacancyDetails(Vacancy vacancy)
            throws FetchErrorException {
        try {
            String details = parseDetailsPage(vacancy);
            return vacancy.setContent(details);
        }
        catch (IOException ex) {
            throw new FetchErrorException("Fetching details error:" +
                    " io exception " + ex.getMessage());
        }
    }


    private List<Vacancy> parseSearchPage(String searchQuery, String city) throws IOException {
        String searchUrl = normalizeQuery(spec.buildSearchUrl(searchQuery, city));
        Document doc = Jsoup.connect(searchUrl).get();
        Elements vacancyElems = doc.select(spec.getSelectorVacancy());
        List<Vacancy> vacancies = new ArrayList<>();
        Log.d(TAG, "Getting search page: " + searchUrl);
        Log.d(TAG, "Found this many vacancy elements on the page: " + vacancyElems.size());
        for (Element vacancyEl: vacancyElems) {
            // since parsing html is a mess we can't really be sure it's gonna work fine
            // so just don't add any vacancies that we've failed to parse
            try {
                Vacancy v = parseVacancy(vacancyEl);
                vacancies.add(v);
            }
            catch (ParseErrorException ex) {
                Log.d(TAG, "Failed to parse vacancy: " + ex.getMessage());
            }
        }
        return vacancies;
    }

    private Vacancy parseVacancy(Element vacancyEl) throws ParseErrorException {
        // make copy of the element because parse actions may remove useful information that
        // we will need later. Ugly but will do for now
        Element el = Jsoup.parse(vacancyEl.outerHtml());
        String city = spec.processCityElement(el.select(spec.getSelectorVacancyCity())).text().trim();
        el = Jsoup.parse(vacancyEl.outerHtml());
        String title = spec.processTitleElement(el.select(spec.getSelectorVacancyTitle())).text().trim();
        String company = vacancyEl.select(spec.getSelectorVacancyCompany()).text().trim();
        String shortDescr = vacancyEl.select(spec.getSelectorVacancyShortDescr()).text().trim();
        String urlRaw = vacancyEl.select(spec.getSelectorVacancyLink()).attr("href");
        String url = getFirstGroup(spec.getRegexRelativeUrl(), urlRaw);
        String externalId = getFirstGroup(spec.getRegexIdVacancy(), url);
        String companyId = getFirstGroup(spec.getRegexIdCompany(), url);
        Log.d(TAG, String.format("Parsed this -- title: %s, city:%s, company: %s, descr: %s, url:%s, " +
                        "externalId: %s, companyId: %s",
                title, city, company, shortDescr, url, externalId, companyId));
        if (!checkStrings(title, city, company, shortDescr, url, externalId, companyId)) {
            throw new ParseErrorException();
        }
        return new Vacancy(-1, externalId, companyId, title, shortDescr, city,
                company, url, website, System.currentTimeMillis(), true);
    }

    private String parseDetailsPage(Vacancy vacancy) throws IOException {
        String relativeUrl = vacancy.getUrl();
        if (!relativeUrl.startsWith("/")) {
            relativeUrl = "/" + relativeUrl;
        }
        String url = spec.getBaseUrl() + relativeUrl;
        Log.d(TAG, "Trying to parse details " + url);
        Document doc = Jsoup.connect(url)
                // it works with ignoreHttpErrors, so we ignore errors what can possibly go wrong
                .timeout(15000).ignoreHttpErrors(true).followRedirects(true)
                .get();
        Elements els = doc.select(spec.getSelectorVacancyDescription());
        if (els.size() > 0) {
            return els.get(0).outerHtml();
        }
        return "";
    }

    /**
     * Check that all given strings are not null and not empty
     */
    private static boolean checkStrings(String... strings) {
        for (String s: strings) {
            if (s == null || s.equals("")) return false;
        }
        return true;
    }

    /**
     * Match given url string against given pattern and return first matched group
     * @param regex pattern used for matching. Must include at least one capturing group
     * @param url url to match
     * @return first matched group or null if not matched or number of matched groups is less than 1
     */
    private static String getFirstGroup(Pattern regex, String url) {
        Log.d(TAG, "getting first group of " + url + " via " + regex.pattern());
        Matcher match = regex.matcher(url);
        if (match.find() && match.groupCount() >= 1) {
            return match.group(1);
        }
        return null;
    }

    private static String normalizeQuery(String query) {
        return query.toLowerCase().replace(" ", "+");
    }
}
