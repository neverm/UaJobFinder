package com.domain.nvm.uajobfinder.data;

public enum Website {

    DOU("dou.ua"), RABOTA("rabota.ua");

    private final String websiteName;

    Website(String value) {
        websiteName = value;
    }

    public String getName() {
        return websiteName;
    }

    public static Website fromString(String text) {

        for (Website w: Website.values()) {
            if (w.websiteName.equalsIgnoreCase(text)) {
                return w;
            }
        }
        throw new IllegalArgumentException("No constant with text " + text + " found");
    }
}
