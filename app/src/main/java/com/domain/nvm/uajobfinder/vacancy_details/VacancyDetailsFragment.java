package com.domain.nvm.uajobfinder.vacancy_details;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.domain.nvm.uajobfinder.R;
import com.domain.nvm.uajobfinder.data.Vacancy;

public class VacancyDetailsFragment extends Fragment implements VacancyDetailsContract.View {

    private static final String ARG_VACANCY = "vacancy";

    private VacancyDetailsContract.Presenter presenter;
    private Vacancy vacancy;

    public static VacancyDetailsFragment newInstance(Vacancy vacancy) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_VACANCY, vacancy);
        VacancyDetailsFragment fragment = new VacancyDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // get vacancy from args
        // populate views with the data
        vacancy = (Vacancy) getArguments().getSerializable(ARG_VACANCY);
        View root = inflater.inflate(R.layout.fragment_vacancy_details, container, false);
        TextView title = (TextView) root.findViewById(R.id.details_title);
        TextView city = (TextView) root.findViewById(R.id.details_city);
        TextView noDescriptionMsg = (TextView) root.findViewById(R.id.details_no_description_message);
        Button openDetails = (Button) root.findViewById(R.id.details_button_open_vacancy);
        WebView details = (WebView) root.findViewById(R.id.details_description);
        title.setText(vacancy.getTitle());
        city.setText(vacancy.getCity());
        if (vacancy.getContent() == null || vacancy.getContent().equals("")) {
            noDescriptionMsg.setVisibility(View.VISIBLE);
            details.setVisibility(View.GONE);
        }
        openDetails.setOnClickListener((view) -> {
            presenter.openVacancyUrl(vacancy);
        });
        String htmlData = "<link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\" />" +
                vacancy.getContent();
        details.loadDataWithBaseURL("file:///android_asset/", htmlData, "text/html", "UTF-8", null);
        return root;
    }

    @Override
    public void setPresenter(VacancyDetailsContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.readVacancy(vacancy);
    }

    @Override
    public void showDetails(Vacancy vacancy) {

    }

    @Override
    public void goToVacancyUrl(Uri url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, url);
        getActivity().startActivity(intent);
    }
}
