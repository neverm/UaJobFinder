package com.domain.nvm.uajobfinder;

public interface BasePresenter {

    void start();
    void detach();
}
