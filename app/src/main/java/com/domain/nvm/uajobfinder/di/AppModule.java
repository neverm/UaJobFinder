package com.domain.nvm.uajobfinder.di;

import android.app.Application;
import android.content.Context;

import com.domain.nvm.uajobfinder.data.VacanciesRepository;
import com.domain.nvm.uajobfinder.data.sync.SyncManager;
import com.domain.nvm.uajobfinder.data.sync.VacanciesSynchronizer;
import com.domain.nvm.uajobfinder.preferences.Preferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    Application app;

    VacanciesRepository repo;
    Preferences preferences;
    SyncManager syncManager;

    public AppModule(Application app) {
        this.app = app;
        preferences = new Preferences(app);
        repo = new VacanciesRepository(app);

        syncManager = new SyncManager(app, preferences, repo);
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return app;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return app.getApplicationContext();
    }

    @Provides
    @Singleton
    SyncManager provideSynchronizer() {
        return syncManager;
    }

    @Provides
    @Singleton
    VacanciesRepository provideRepo() {
        return repo;
    }

    @Provides
    @Singleton
    Preferences providePreferences() {
        return preferences;
    }
}
