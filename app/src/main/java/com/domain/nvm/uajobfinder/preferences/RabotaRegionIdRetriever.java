package com.domain.nvm.uajobfinder.preferences;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class RabotaRegionIdRetriever {

    public static final int NOT_FOUND_REGION = -1;

    private static final String TAG = RabotaRegionIdRetriever.class.getSimpleName();
    private static final String LOCAL_ENTRIES_FILE = "rabota_region_ids.json";

    private static Map<String, Integer> entries;

    /**
     * Smart guys at rabota.ua know that town names change every other day, so they use
     * internal id to identify a region.
     * Unfortunately for us this means we have to either maintain a copy of mapping of region names
     * to id locally, or retrieve it at runtime from rabota.ua.
     * For now, let's settle on local copy and retrieve region id from there
     * @param regionName name of the city
     * @return id of the region to be used in queries to rabota.ua
     */
    public static int getRegionId(String regionName, Context context) {
        if (entries == null) {
            entries = getRegionIdEntries(context);
        }
        if (entries.containsKey(regionName)) {
            return entries.get(regionName.toLowerCase());
        }
        else {
            return NOT_FOUND_REGION;
        }
    }


    private static Map<String, Integer> getRegionIdEntries(Context context) {
        Map<String, Integer> res = new HashMap<>();
        String entriesText = getLocalEntriesJson(context);
        try {
            JSONArray fileEntries = new JSONArray(entriesText);
            for (int i = 0; i < fileEntries.length(); i++) {
                JSONObject entry = fileEntries.getJSONObject(i);
                String key = entry.keys().next();
                res.put(key.toLowerCase(), entry.getInt(key));
            }
        }
        catch (JSONException ex) {
            // this should never happen with my perfect hand-crafted JSON tbh
            Log.e(TAG, "Error while parsing json from local regions ids", ex);
        }
        return res;
    }

    private static String getLocalEntriesJson(Context context) {
        try {

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    context.getAssets().open(LOCAL_ENTRIES_FILE)
            ));
            StringBuilder result = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }
            return result.toString();
        }
        catch (IOException ex) {
            Log.e(TAG, "Error: can't open local regions ids file", ex);
            return "";
        }
    }
}
