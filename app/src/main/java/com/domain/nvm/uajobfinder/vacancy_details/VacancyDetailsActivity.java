package com.domain.nvm.uajobfinder.vacancy_details;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.domain.nvm.uajobfinder.JobFinderApplication;
import com.domain.nvm.uajobfinder.R;
import com.domain.nvm.uajobfinder.data.Vacancy;
import com.domain.nvm.uajobfinder.di.AppComponent;
import com.domain.nvm.uajobfinder.di.DaggerVacancyDetailsComponent;
import com.domain.nvm.uajobfinder.di.VacancyDetailsComponent;
import com.domain.nvm.uajobfinder.di.VacancyDetailsModule;

import javax.inject.Inject;

public class VacancyDetailsActivity extends AppCompatActivity {

    private static final String EXTRA_VACANCY = "vacancy";

    @Inject VacancyDetailsPresenter presenter;

    public static Intent makeIntent(Context context, Vacancy vacancy) {
        Intent i = new Intent(context, VacancyDetailsActivity.class);
        i.putExtra(EXTRA_VACANCY, vacancy);
        return i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Vacancy vacancy = (Vacancy) getIntent().getSerializableExtra(EXTRA_VACANCY);
        setContentView(R.layout.single_fragment_activity);

        VacancyDetailsFragment fragment =
                (VacancyDetailsFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.fragment_container);

        if (fragment == null) {
            fragment = VacancyDetailsFragment.newInstance(vacancy);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, fragment)
                    .commit();
        }

        AppComponent appComponent = ((JobFinderApplication) getApplication()).getAppComponent();
        DaggerVacancyDetailsComponent.builder()
                .appComponent(appComponent)
                .vacancyDetailsModule(new VacancyDetailsModule(fragment))
                .build()
                .inject(this);
    }
}
