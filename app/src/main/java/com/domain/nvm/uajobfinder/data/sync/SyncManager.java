package com.domain.nvm.uajobfinder.data.sync;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;

import com.domain.nvm.uajobfinder.Utils;
import com.domain.nvm.uajobfinder.data.VacanciesRepository;
import com.domain.nvm.uajobfinder.data.Vacancy;
import com.domain.nvm.uajobfinder.data.Website;
import com.domain.nvm.uajobfinder.preferences.Preferences;


import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class SyncManager implements Preferences.QuerySettingObserver {

    private static final String TAG = SyncManager.class.getSimpleName();
    private static final String PREF_KEY_LAST_SYNC = "last_sync_time";

    private SharedPreferences sharedPrefs;
    private Preferences appPrefs;
    private Context context;
    private boolean isSyncing;
    private VacanciesSynchronizer synchronizer;

    @Inject
    public SyncManager(Context context, Preferences appPrefs, VacanciesRepository repository) {
        this.context = context.getApplicationContext();
        this.sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this.context);
        this.appPrefs = appPrefs;
        appPrefs.addOnPrefsChangedListener(this);
        this.synchronizer = new VacanciesSynchronizer(repository);
    }

    /**
     * Perform synchronization with remote sources as in synchronizeAndGetNew and then
     * consume the observables
     * This method should be used when the result of synchronization is not used
     * @param force
     */
    public void synchronize(boolean force) {
        synchronizeAndGetNew(force)
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }

    /**
     * Perform synchronization if the time passed from the last sync is more than synchronization
     * frequency in settings or if force param is true.
     * @param force force synchronization to be performed now
     * @return observable of new vacancies that were not present in db before sync
     */
    public Observable<Vacancy> synchronizeAndGetNew(boolean force) {
        long lastSync = sharedPrefs.getLong(PREF_KEY_LAST_SYNC, 0);
        long now = System.currentTimeMillis();
        boolean shouldSynchronize = force || (now - lastSync) > appPrefs.getSyncFrequency();
        if (isSyncing || !Utils.isNetworkAvailable(context) || !shouldSynchronize) {
            return Observable.empty();
        }
        Log.d(TAG, "Synchronizing");
        isSyncing = true;
        Observable<Vacancy> dou = syncWebsite(Website.DOU);
        Observable<Vacancy> rabota = syncWebsite(Website.RABOTA);
        return Observable.mergeDelayError(dou, rabota)
                .doOnComplete(() -> {
                    updateLastSyncTime();
                    isSyncing = false;
                })
                .doOnError(err -> Log.d(TAG, "failed to synchronize "+err.getMessage()));
    }

    /**
     * Setup automatic synchronization with respect to current sync settings.
     */
    public void setupAutoSync() {
        long syncFrequency = appPrefs.getSyncFrequency();
        boolean isAlarmSet = SyncService.makePendingIntent(context,
                PendingIntent.FLAG_NO_CREATE) != null;
        if (syncFrequency == 0 || isAlarmSet) {
            // no need to setup synchronization
            return;
        }
        PendingIntent pi = SyncService.makePendingIntent(context, 0);
        AlarmManager alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        alarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + syncFrequency, syncFrequency, pi);
    }

    private Observable<Vacancy> syncWebsite(Website website) {
        if (!appPrefs.getSearchQuery(website).equals(Preferences.EMPTY_QUERY)) {
            return synchronizer.syncWebsite(website, appPrefs.getSearchQuery(website),
                    appPrefs.getCity(website));
        }
        else {
            return Observable.empty();
        }
    }

    @Override
    public void onSearchQueryChanged(Website website) {
        synchronize(true);
    }

    @Override
    public void onSyncFrequencyChanged(long newFrequency) {
        if (newFrequency == 0) {
            cancelAutoSync();
        }
        else {
            setupAutoSync();
            synchronize(false);
        }
    }

    private void cancelAutoSync() {
        PendingIntent pi = SyncService.makePendingIntent(context, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pi);
        // cancel pi so that check with FLAG_NO_CREATE would return false
        pi.cancel();
    }

    private void updateLastSyncTime() {
        sharedPrefs.edit().putLong(PREF_KEY_LAST_SYNC, System.currentTimeMillis()).apply();
    }
}
