package com.domain.nvm.uajobfinder;

public interface BaseView<P> {

    void setPresenter(P presenter);

}
