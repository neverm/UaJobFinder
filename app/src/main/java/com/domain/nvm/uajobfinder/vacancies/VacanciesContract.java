package com.domain.nvm.uajobfinder.vacancies;

import android.content.Context;
import android.support.annotation.NonNull;

import com.domain.nvm.uajobfinder.BasePresenter;
import com.domain.nvm.uajobfinder.BaseView;
import com.domain.nvm.uajobfinder.data.Vacancy;

import java.util.List;
// todo: suspiciously similar to data.db.VacancyContract, rename to something else
public interface VacanciesContract {

    interface View extends BaseView<Presenter> {

        void showVacancies(List<Vacancy> vacancyList);
        void showNoVacancies();
        void showVacancyDetailsUI(Vacancy vacancy);
        void showNetworkNotAvailableMessage();
        void showLoading();
        void hideLoading();
    }

    interface Presenter extends BasePresenter {
        void loadVacancies();
        void openVacancyDetails(@NonNull Vacancy vacancy);
        void requestSync(Context context);
        void requestClearDb();
    }
}
