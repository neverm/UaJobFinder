package com.domain.nvm.uajobfinder.data.fetch;

import org.jsoup.select.Elements;

import java.util.regex.Pattern;

public interface ParseSpec {

    /**
     * Additional actions that fetcher may want to perform on parsed elements
     * Caution: fetcher may mutate data passed to it
     */
    default Elements processCityElement(Elements city) {
        return city;
    }
    default Elements processTitleElement(Elements title) {
        return title;
    }

    /**
     * Build search url to the page will the vacancy list corresponding to the provided
     * search query
     * @param searchQuery
     * @param city
     * @return
     */
    String buildSearchUrl(String searchQuery, String city);
    String getBaseUrl();

    // different selectors that are used to extract bit of information from html page
    String getSelectorVacancy();
    String getSelectorVacancyTitle();
    String getSelectorVacancyCity();
    String getSelectorVacancyCompany();
    String getSelectorVacancyShortDescr();
    String getSelectorVacancyLink();
    String getSelectorVacancyDescription();
    String getBaseSearchUrl();
    Pattern getRegexRelativeUrl();
    Pattern getRegexIdCompany();
    Pattern getRegexIdVacancy();
}