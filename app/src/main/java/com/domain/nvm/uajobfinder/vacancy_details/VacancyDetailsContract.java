package com.domain.nvm.uajobfinder.vacancy_details;

import android.net.Uri;
import android.support.annotation.NonNull;

import com.domain.nvm.uajobfinder.BasePresenter;
import com.domain.nvm.uajobfinder.BaseView;
import com.domain.nvm.uajobfinder.data.Vacancy;

public interface VacancyDetailsContract {

    interface View extends BaseView<Presenter> {
        void showDetails(Vacancy vacancy);
        void goToVacancyUrl(Uri url);
    }

    interface Presenter extends BasePresenter {
        void openVacancyUrl(@NonNull Vacancy vacancy);
        void readVacancy(@NonNull Vacancy vacancy);
    }
}
