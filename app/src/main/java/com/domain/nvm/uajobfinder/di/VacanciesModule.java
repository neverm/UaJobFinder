package com.domain.nvm.uajobfinder.di;

import com.domain.nvm.uajobfinder.vacancies.VacanciesContract;

import dagger.Module;
import dagger.Provides;

@Module
public class VacanciesModule {

    private VacanciesContract.View view;

    public VacanciesModule(VacanciesContract.View v) {
        view = v;
    }

    @Provides
    @ActivityScope
    VacanciesContract.View provideView() {
        return view;
    }
}
