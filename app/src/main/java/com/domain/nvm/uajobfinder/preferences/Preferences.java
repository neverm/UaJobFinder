package com.domain.nvm.uajobfinder.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.StringRes;
import android.util.Log;

import com.domain.nvm.uajobfinder.R;
import com.domain.nvm.uajobfinder.data.Website;


import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class Preferences {

    public interface QuerySettingObserver {
        void onSearchQueryChanged(Website website);
        void onSyncFrequencyChanged(long newFrequency);
    }

    public static final String EMPTY_QUERY = "";
    public static final String ALL_CITIES = "";

    private static final String RABOTA_REGION_ID_KEY = "prefs_rabota_region_id_key";
    private static final int RABOTA_REGION_ID_ALL_REGIONS = 0;

    private SharedPreferences prefs;
    private Context context;
    private List<QuerySettingObserver> observers;

    @Inject
    public Preferences(Context ctx) {
        context = ctx.getApplicationContext();
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        observers = new ArrayList<>();
    }

    public void addOnPrefsChangedListener(QuerySettingObserver observer) {
        observers.add(observer);
    }

    public void removeOnPrefsChangedListener(QuerySettingObserver observer) {
        observers.remove(observer);
    }

    public void notifyQueryChanged(Website website) {
        for (QuerySettingObserver observer: observers) {
            observer.onSearchQueryChanged(website);
        }
    }

    public void notifySyncFrequencyChanged(long newFrequency) {
        for (QuerySettingObserver observer: observers) {
            observer.onSyncFrequencyChanged(newFrequency);
        }
    }

    public String getSearchQuery(Website website) {
        switch (website) {
            case RABOTA:
                return getQuery(R.string.preference_key_rabota_ua_query);
            case DOU:
            default:
                return getQuery(R.string.preference_key_dou_ua_searchquery);
        }
    }

    public long getSyncFrequency() {
        String val = prefs.getString(context.getString(R.string.preference_key_sync_frequency), "0");
        return Long.parseLong(val);
    }

    private String getQuery(@StringRes int keyId) {
        return prefs.getString(context.getString(keyId), EMPTY_QUERY);
    }

    /**
     * Get current city setting with respect to the website
     * Some websites (like rabota.ua) use id to specify the city while others (like dou.ua)
     * just use the name of the city
     * @param website
     * @return city string ready to be inserted into search url
     */
    public String getCity(Website website) {
        switch (website) {
            case DOU:
                return getCitySetting();
            case RABOTA:
            default:
                int regionId = getRabotaUaRegionId();
                if (regionId == RabotaRegionIdRetriever.NOT_FOUND_REGION) {
                    return Integer.toString(RABOTA_REGION_ID_ALL_REGIONS);
                }
                else {
                    return Integer.toString(regionId);
                }
        }
    }

    private int getRabotaUaRegionId() {
        return prefs.getInt(RABOTA_REGION_ID_KEY, RabotaRegionIdRetriever.NOT_FOUND_REGION);
    }

    private String getCitySetting() {
        return prefs.getString(context.getString(R.string.preference_key_city), ALL_CITIES);
    }

    /**
     * update rabota.ua region id with respect to the new setting
     */
    public void onCityChanged() {
        String city = getCitySetting();
        int id = RABOTA_REGION_ID_ALL_REGIONS;
        if (!city.equals("")) {
            id = RabotaRegionIdRetriever.getRegionId(city, context);
        }
        prefs.edit().putInt(RABOTA_REGION_ID_KEY, id).apply();
    }
}
