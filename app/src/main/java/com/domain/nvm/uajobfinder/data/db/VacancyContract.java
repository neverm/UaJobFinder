package com.domain.nvm.uajobfinder.data.db;

import android.provider.BaseColumns;

public class VacancyContract {

    public static final class VacancyEntry implements BaseColumns {

        public static final String TABLE_NAME = "vacancies";

        public static final String COL_EXTERNAL_ID = "external_id";
        public static final String COL_COMPANY_ID = "company_id";
        public static final String COL_TITLE = "title";
        public static final String COL_CONTENT = "content";
        public static final String COL_CITY = "city";
        public static final String COL_COMPANY_NAME = "company_name";
        public static final String COL_URL = "url";
        public static final String COL_WEBSITE = "website";
        public static final String COL_LAST_FETCH_TIME = "last_fetch_time";
        public static final String COL_SALARY = "salary";
        public static final String COL_IS_NEW = "is_new";
    }
}
