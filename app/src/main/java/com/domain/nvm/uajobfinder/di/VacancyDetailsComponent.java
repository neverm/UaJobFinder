package com.domain.nvm.uajobfinder.di;

import com.domain.nvm.uajobfinder.vacancy_details.VacancyDetailsActivity;

import dagger.Component;

@Component(modules = {VacancyDetailsModule.class}, dependencies = {AppComponent.class})
@ActivityScope
public interface VacancyDetailsComponent {
    void inject(VacancyDetailsActivity activity);
}
