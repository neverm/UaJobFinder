package com.domain.nvm.uajobfinder.preferences;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.domain.nvm.uajobfinder.JobFinderApplication;
import com.domain.nvm.uajobfinder.R;
import com.domain.nvm.uajobfinder.data.Website;

import javax.inject.Inject;

public class PreferencesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.single_fragment_activity);
        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, new SettingsFragment())
                .commit();
    }

    public static class SettingsFragment extends PreferenceFragment
        implements SharedPreferences.OnSharedPreferenceChangeListener {

        @Inject
        Preferences prefs;

        private String keyDouQuery, keyRabotaQuery, keySyncFrequency, keyCity;

        @Override
        public void onActivityCreated(@Nullable Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            ((JobFinderApplication) getActivity().getApplication())
                    .getAppComponent().inject(this);
            keyDouQuery = getActivity().getString(R.string.preference_key_dou_ua_searchquery);
            keyRabotaQuery = getActivity().getString(R.string.preference_key_rabota_ua_query);
            keySyncFrequency = getActivity().getString(R.string.preference_key_sync_frequency);
            keyCity = getActivity().getString(R.string.preference_key_city);
            addPreferencesFromResource(R.xml.preferences);
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if (key.equals(keyDouQuery)) {
                prefs.notifyQueryChanged(Website.DOU);
            }
            else if (key.equals(keyRabotaQuery)) {
                prefs.notifyQueryChanged(Website.RABOTA);
            }
            else if (key.equals(keySyncFrequency)) {
                long val = Long.parseLong(sharedPreferences
                        .getString(keySyncFrequency, "0"));
                prefs.notifySyncFrequencyChanged(val);
            }
            else if (key.equals(keyCity)) {
                prefs.onCityChanged();
            }
        }

        @Override
        public void onResume() {
            super.onResume();
            getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onPause() {
            getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
            super.onPause();
        }
    }
}
