package com.domain.nvm.uajobfinder.data.sync;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.domain.nvm.uajobfinder.JobFinderApplication;
import com.domain.nvm.uajobfinder.R;
import com.domain.nvm.uajobfinder.data.Vacancy;
import com.domain.nvm.uajobfinder.vacancies.VacanciesActivity;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * This synchronization service serves two roles:
 * 1. It's called by android alarm service to automatically synchronize local data with remote
 * 2. It's called on device boot to register automatic synchronization
 */

public class SyncService extends Service {

    private static final String TAG = SyncService.class.getSimpleName();

    private static final int NEW_VACANCIES_NOTIFICATION_ID = 1;

    private static final int SYNC_BOOT = 1;
    private static final int SYNC_REGULAR = 2;
    private static final String EXTRA_SYNC_TYPE = "sync_type";

    @Inject
    SyncManager manager;

    public static PendingIntent makePendingIntent(Context context, int flags) {
        Intent syncService = makeSyncIntent(context);
        return PendingIntent.getService(context, 0, syncService, flags);
    }

    public static Intent makeBootSyncIntent(Context context) {
        return makeIntent(context, SYNC_BOOT);
    }

    public static Intent makeSyncIntent(Context context) {
        return makeIntent(context, SYNC_REGULAR);
    }

    private static Intent makeIntent(Context context, int syncType) {
        Intent i = new Intent(context, SyncService.class);
        i.putExtra(EXTRA_SYNC_TYPE, syncType);
        return i;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ((JobFinderApplication) getApplication()).getAppComponent().inject(this);
        Log.d(TAG, "SyncService::onStartCommand");
        Bundle extras = intent.getExtras();
        if (extras != null && extras.getInt(EXTRA_SYNC_TYPE, SYNC_REGULAR) == SYNC_BOOT) {
            manager.setupAutoSync();
        }
        manager.synchronizeAndGetNew(false)
                .toList()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(this::notifyNew); // todo: log error somewhere
        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void notifyNew(List<Vacancy> newVacancies) {
        Log.d(TAG, "notifying about " + newVacancies.size() + " new vacancies");
        if (newVacancies.size() > 0) {
            String title = getString(R.string.notification_new_vacancies_title);
            String content = String.format(getString(R.string.notification_new_vacancies_content),
                    Integer.toString(newVacancies.size()));
            NotificationCompat.Builder builder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle(title)
                            .setAutoCancel(true)
                            .setContentText(content);
            Intent resultIntent = new Intent(this, VacanciesActivity.class);
            PendingIntent resultPendingIntent =
                    PendingIntent.getActivity(
                            this,
                            0,
                            resultIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );
            builder.setContentIntent(resultPendingIntent);
            // Gets an instance of the NotificationManager service
            NotificationManager mNotifyMgr =
                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            // Builds the notification and issues it.
            mNotifyMgr.notify(NEW_VACANCIES_NOTIFICATION_ID, builder.build());
        }
    }
}
