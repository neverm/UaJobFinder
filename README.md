# UaJobFinder

### Description
A simple application that keeps track of vacancies on popular Ukrainian job websites and notifies user when there are new vacancies posted.
As of today, two websites are supported: dou.ua and rabota.ua.

### Usage
Set search query setting (eg "android developer") for the websites you want to track and set synchronization frequency setting.
